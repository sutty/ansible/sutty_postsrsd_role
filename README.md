Sutty PostSRSd
==============

SRS is a technique for e-mail forwarding that doesn't break SPF
checks by rewriting the sender.  This prevents other e-mail servers to
bounce our e-mail or catalog it as spam.

The secret is distributed because it's checked during bounces and
a bounce can arrive between container restarts and maybe to any node.

Role Variables
--------------

Needs a `domain` from which addresses are rewritten and a Docker
`network`.

* `domain`: Main domain
* `network`: Docker network
* `data`: Shared storage outside the container
* `dest`: Secret location (default: "/etc/postsrsd.secret")

Example Playbook
----------------

```yaml
- name: "postsrsd"
  include_role:
    name: "sutty-postsrsd"
  vars:
    network: "{{ docker_network }}"
    domain: "{{ sutty }}"
```

License
-------

MIT-Antifa
